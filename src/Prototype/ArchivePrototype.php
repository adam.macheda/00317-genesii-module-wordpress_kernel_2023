<?php

namespace Genesii\Kernel\Prototype;

use Genesii\Kernel\Utils\Slugify;

class ArchivePrototype extends AbstractPrototype implements PrototypeInterface {

    use Slugify;

    public function getFileName(): string
    {
        return 'archive-' . $this->get('slug') . '.php';
    }

    public function getPhpCode(): string 
    {
        return ""
        .'<?php'."\n"
        .'/**'."\n"
        .'* Archive page template pour ' . $this->getName() ."\n"
        .'*/'."\n"
        .'get_header();'."\n"
        .'?>'."\n"
        ."\n"
        .'<?php while (have_posts()) : ?>'."\n"
        .'    <?php the_title(); ?>'."\n"
        .'    <?php the_post(); ?>'."\n"
        .'<?php endwhile; ?>'."\n"
        ."\n"
        ."<?php\n"
        ."get_footer();";
    }
}