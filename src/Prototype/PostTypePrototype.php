<?php

namespace Genesii\Kernel\Prototype;

class PostTypePrototype extends AbstractPrototype implements PrototypeInterface {

    public function getFileName(): string
    {
        return $this->getName() . '.php';
    }

    public function getPhpCode(): string 
    {
        return ""
        ."<?php\n"
        ."\n"
        ."namespace Genesii\PostType;\n"
        ."\n"
        ."use Genesii\Kernel\PostType\AbstractPostType;\n"
        ."\n"
        ."final class " . $this->getName() . " extends AbstractPostType {\n"
        ."\n"
        ."    const SLUG = \"" . $this->get('slug') . "\";\n"
        ."    const REWRITE_SLUG = \"" . $this->get('slug') . "\";\n"
        ."    const SINGULAR_NAME = \"" . $this->get('singulier') . "\";\n"
        ."    const PLURIAL_NAME = \"" . $this->get('pluriel') . "\";\n"
        ."    const FEMININE = " . ($this->get('feminin') ? 'true' : 'false') . ";\n"
        ."    const MENU_POSITION = 4;\n"
        ."    const MENU_ICON = '" . $this->get('icone') . "';\n"
        ."    const PUBLIC = true;\n"
        ."    const REMOVE_EDITOR = true;\n"
        ."    const PUBLICLY_QUERYABLE = true;\n"
        ."    const SHOW_UI = true;\n"
        ."    const SHOW_IN_MENU = null;\n"
        ."    const SHOW_IN_REST = true;\n"
        ."    const QUERY_VAR = true;\n"
        ."    const CAPABILITY_TYPE = 'post';\n"
        ."    const HAS_ARCHIVE = true;\n"
        ."    const HIERARCHICAL = true;\n"
        ."    const SUPPORTS = ['title'];\n"
        ."\n"
        ."    public static function hooks(): void \n"
        ."    {\n"
        ."        // ...\n"
        ."        // ici, déclaration des hooks associés à ce type de contenu, exemple :\n"
        ."\n"
        ."        /* \n"
        ."        add_action('mon_hook', [" . $this->getName() . "::class, 'maMethodeStatique']);\n"
        ."        */\n"
        ."    }\n"
        ."\n"
        ."    public static function save(".'$id'."): void\n"
        ."    {\n"
        ."        // ...\n"
        ."        // action à effectuer juste après l'enregistrement du post, exemple pour \n"
        ."        // définir une meta custom au post qui vient d'être créé/enregistré :\n"
        ."\n"
        ."        /* \n"
        .'        $' . lcfirst($this->getName()) . ' = new ' . $this->getName() . '($id);'."\n"
        .'        $' . lcfirst($this->getName()) . '->setMeta(\'ma_custom_meta\', \'valeur\');'."\n"
        ."\n"
        .'        var_dump($' . lcfirst($this->getName()) . '->getMeta(\'ma_custom_meta\'));'."\n"
        ."        exit();\n"
        ."        */\n"
        ."    }\n"
        ."}\n"
        ."\n"
        ."";
    }
}