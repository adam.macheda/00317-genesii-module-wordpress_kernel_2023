<?php

namespace Genesii\Kernel\Prototype;

interface PrototypeInterface
{
    public function getFileName(): string;
    public function getPhpCode(): string;
}
