<?php

namespace Genesii\Kernel\Prototype;

class ServicePrototype extends AbstractPrototype implements PrototypeInterface {

    public function getFileName(): string
    {
        return $this->getName() . '.php';
    }

    public function getPhpCode(): string 
    {
        return ""
        ."<?php\n\n"
        ."namespace Genesii\Service;\n\n"
        ."use Genesii\Kernel\Service\AbstractService;\n"
        ."\n"
        ."final class ". $this->getName() ." extends AbstractService {\n\n"
        ."    protected function hooks(): void \n"
        ."    {\n"
        ."        // ...\n"
        ."        // ici, définir les hooks et les méthodes à appeler dans ce service, exemple :\n"
        ."\n"
        .'        /* add_action(\'init\', [&$this, \'monAction\']); */'."\n"
        ."    }\n"
        ."\n"
        ."    /*\n"
        ."    public function monAction() \n"
        ."    {\n"
        ."        // ...\n"
        ."        // action personnalisée à effectuer sur le hook dont l'action est définie ci-dessus\n"
        ."    }\n"
        ."    */\n"
        ."}\n"
        ."";
    }
}