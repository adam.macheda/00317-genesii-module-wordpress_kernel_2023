<?php

namespace Genesii\Kernel\Prototype;

class TaxonomyPrototype extends AbstractPrototype implements PrototypeInterface {

    public function getFileName(): string
    {
        return $this->getName() . '.php';
    }

    public function getPhpCode(): string 
    {
        return ""
        ."<?php\n"
        ."\n"
        ."namespace Genesii\Taxonomy;\n"
        ."\n"
        ."use Genesii\Kernel\Taxonomy\AbstractTaxonomy;\n"
        ."\n"
        ."final class " . $this->getName() . " extends AbstractTaxonomy {\n"
        ."\n"
        ."    const POST_TYPE = \"" . $this->get('cpt') . "\";\n"
        ."    const SLUG = \"" . $this->get('slug') . "\";\n"
        ."    const SINGULAR_NAME = \"" . $this->get('singulier') . "\";\n"
        ."    const PLURIAL_NAME = \"" . $this->get('pluriel') . "\";\n"
        ."    const FEMININE = " . ($this->get('feminin') ? 'true' : 'false') . ";\n"
        ."    const META_BOX_CB = false;\n"
        ."    const REWRITE_SLUG = \"" . $this->get('slug') . "\";\n"
        ."    const REWRITE_WITH_FRONT = false;\n"
        ."    const REWRITE_HIERARCHICAL = true;\n"
        ."}\n"
        ."";
    }
}