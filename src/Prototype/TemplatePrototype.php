<?php

namespace Genesii\Kernel\Prototype;

use Genesii\Kernel\Utils\Slugify;

class TemplatePrototype extends AbstractPrototype implements PrototypeInterface {

    use Slugify;

    public function getFileName(): string
    {
        return 'page-' . trim($this->slugify($this->getName())) . '.php';
    }

    public function getPhpCode(): string 
    {
        return ""
        ."<?php\n"
        ."/*\n"
        ."    Template Name: " . $this->getName() . "\n"
        ."*/\n\n"
        ."get_header();\n"
        ."?>\n\n"
        ."<!-- ... -->\n"
        ."<!-- ici mon contenu HTML/PHP -->\n"
        ."<?php the_content(); ?>\n\n"
        ."<?php\n"
        ."get_footer();";
    }
}