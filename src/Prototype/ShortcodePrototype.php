<?php

namespace Genesii\Kernel\Prototype;

class ShortcodePrototype extends AbstractPrototype implements PrototypeInterface {

    public function getFileName(): string
    {
        return $this->getName() . '.php';
    }

    public function getPhpCode(): string 
    {
        return ""
        ."<?php\n\n"
        ."namespace Genesii\Shortcode;\n\n"
        ."use Genesii\Kernel\Shortcode\AbstractShortcode;\n"
        ."\n"
        ."final class ". $this->getName() ." extends AbstractShortcode {\n\n"
        .'    const CODE = "' . $this->get('code') . '";'. "\n\n"
        ."    protected function do(?array " . '$args' . "): void {\n"
        ."        // ...\n"
        ."        // ici, actions à faire à l'utilisation du shortcode\n"
        ."    }\n"
        ."}\n"
        ."";
    }
}