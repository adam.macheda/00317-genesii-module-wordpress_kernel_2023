<?php

namespace Genesii\Kernel\Prototype;

use Genesii\Kernel\Utils\Slugify;

class SinglePrototype extends AbstractPrototype implements PrototypeInterface {

    use Slugify;

    public function getFileName(): string
    {
        return 'single-' . $this->get('slug') . '.php';
    }

    public function getPhpCode(): string 
    {
        return ""
        .'<?php'."\n"
        .'/**'."\n"
        .'* Single page template pour ' . $this->getName() ."\n"
        .'*/'."\n"
        .'get_header();'."\n"
        .'?>'."\n"
        ."\n"
        .'<article id="' . $this->get('slug') . '_<?php the_ID(); ?>" <?php post_class(); ?>>'."\n"
        .'    <header>'."\n"
        .'        <h1><?php the_title(); ?></h1>'."\n"
        .'    </header>'."\n"
        ."\n"
        .'    <div>'."\n"
        .'        <?php the_content(); ?>'."\n"
        .'    </div>'."\n"
        .'</article>'."\n\n"
        ."<?php\n"
        ."get_footer();";
    }
}