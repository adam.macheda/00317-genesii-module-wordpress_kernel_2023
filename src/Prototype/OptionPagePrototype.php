<?php

namespace Genesii\Kernel\Prototype;

class OptionPagePrototype extends AbstractPrototype implements PrototypeInterface {

    public function getFileName(): string
    {
        return $this->getName() . '.php';
    }

    public function getPhpCode(): string 
    {
        return ""
        ."<?php\n\n"
        ."namespace Genesii\OptionPage;\n\n"
        ."use Genesii\Kernel\OptionPage\AbstractOptionPage;\n"
        ."\n"
        ."final class ". $this->getName() ." extends AbstractOptionPage {\n\n"
        .'    const TITLE = "' . $this->get('title') . '";'. "\n"
        .'    const SLUG = "' . $this->get('slug') . '";'. "\n"
        .'    const PARENT_SLUG = ' . ($this->get('parent_slug') != null ? '"' . $this->get('parent_slug') . '"' : 'null') . ';'. "\n"
        ."}\n"
        ."";
    }
}