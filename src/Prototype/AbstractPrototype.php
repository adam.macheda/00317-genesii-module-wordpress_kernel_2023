<?php

namespace Genesii\Kernel\Prototype;

abstract class AbstractPrototype {

    protected $name = '';
    protected $data = [];

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return ucfirst($this->name);
    }

    public function setData(array $data): void
    {
        $this->data = $data;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function get(string $key): ?string
    {
        if(!empty($this->data) && !empty($this->data[$key]))
            return $this->data[$key];

        return null;
    }
}