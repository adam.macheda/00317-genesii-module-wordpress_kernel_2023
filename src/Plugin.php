<?php

namespace Genesii\Kernel;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Yaml;

use Composer\Autoload\ClassLoader;

class Plugin {

    protected string $name;
    protected string $path;
    protected array $shortcodes = [];
    protected array $services = [];
    protected array $postTypes = [];
    protected array $taxonomies = [];
    protected array $optionPages = [];

    public function __construct(string $path)
    {
        $this->path = str_replace('\\', '/', $path);
        
        $this->loadAttributes();
        $this->loadPostTypes();
        $this->loadTaxonomies();
        $this->loadAcf();
        $this->loadShortcodes();
        $this->loadCSS();
        $this->loadJS();
        $this->loadTemplates();
        $this->loadServices();
        $this->loadOptionPages();
    }

    protected function loadAttributes(): void
    {
        $this->name = basename($this->path);
    }

    public function loadPostTypes(): void
    {
        $finder = new Finder();
        $filesystem = new Filesystem();

        $path = $this->path . '/src/PostType';

        if($filesystem->exists($path)) {
            $finder->files()->in($path)->name('*.php');

            if ($finder->hasResults()) {
                foreach ($finder as $file) {
                    $className = 'Genesii\\PostType\\' . str_replace('.php', '', $file->getFileName());

                    $this->postTypes[] = $className::init();
                }
            }
        }
    }

    public function loadTaxonomies(): void
    {
        $finder = new Finder();
        $filesystem = new Filesystem();

        $path = $this->path . '/src/Taxonomy';

        if($filesystem->exists($path)) {
            $finder->files()->in($path)->name('*.php');

            if ($finder->hasResults()) {
                foreach ($finder as $file) {
                    $className = 'Genesii\\Taxonomy\\' . str_replace('.php', '', $file->getFileName());

                    $this->taxonomies[] = $className::init();
                }
            }
        }
    }

    public function loadShortcodes(): void
    {
        $finder = new Finder();
        $filesystem = new Filesystem();

        $path = $this->path . '/src/Shortcode';

        if($filesystem->exists($path)) {
            $finder->files()->in($path)->name('*.php');

            if ($finder->hasResults()) {
                foreach ($finder as $file) {
                    $className = 'Genesii\\Shortcode\\' . str_replace('.php', '', $file->getFileName());

                    $this->shortcodes[] = new $className($this->path);
                }
            }
        }
    }

    public function loadCSS(): void
    {
        $absolutePath = $this->path . '/templates/css';

        // Patch Bedrock
        if(strpos($absolutePath, '/web/app/plugins/') !== false) {
            list($absoluteDir, $relativePath) = explode('web/app', $absolutePath);
            $relativePath = '/../app' . $relativePath;
        } else {
            list($absoluteDir, $relativePath) = explode('wp-content', $absolutePath);
            $relativePath = '/wp-content/' . $relativePath;
        }

        if(file_exists($absolutePath . '/front.css')) {            
            add_action('wp_enqueue_scripts', function() use ($relativePath) {
                wp_enqueue_style('front-style-' . $this->name, $relativePath . '/front.css');
            });
        }

        if(file_exists($absolutePath . '/admin.css')) {
            add_action('admin_print_styles', function() use ($relativePath) {
                wp_enqueue_style('admin-style-' . $this->name, $relativePath . '/admin.css');
            });
        }
    }

    public function loadJS(): void
    {
        $absolutePath = $this->path . '/templates/js';
        
        // Patch Bedrock
        if(strpos($absolutePath, '/web/app/plugins/') !== false) {
            list($absoluteDir, $relativePath) = explode('web/app', $absolutePath);
            $relativePath = '/../app' . $relativePath;
        } else {
            list($absoluteDir, $relativePath) = explode('wp-content', $absolutePath);
            $relativePath = '/wp-content/' . $relativePath;
        }

        if(file_exists($absolutePath . '/front.js')) {            
            add_action('wp_enqueue_scripts', function() use ($relativePath) {
                wp_enqueue_script('front-script-' . $this->name, $relativePath . '/front.js', ['jquery']);
            });
        }

        if(file_exists($absolutePath . '/admin.js')) {
            add_action('admin_print_scripts', function() use ($relativePath) {
                wp_enqueue_script('admin-script-' . $this->name, $relativePath . '/admin.js', ['jquery']);
            });
        }
    }

    public function loadTemplates(): void
    {
        $finder = new Finder();
        $filesystem = new Filesystem();

        $path = $this->path . '/templates';

        add_filter('theme_page_templates', function($templates) use ($finder, $filesystem, $path) {
            if($filesystem->exists($path)) {
                $finder->files()->depth(0)->in($path)->name('*.php');
    
                if ($finder->hasResults()) {
                    foreach ($finder as $file) {
                        if(str_starts_with($file->getFilename(), 'page-') && preg_match('|Template Name:(.*)$|mi', file_get_contents($file->getPathname()), $header)) {
                            if(defined('ABSPATH')) {
                                $key = str_replace('\\', '/', str_replace(ABSPATH, '', $file->getPathname()));
                            } else {
                                $key = $file->getPathname();
                            }

                            if(count(explode('wp-content', $key)) > 1) $key = 'wp-content' . explode('wp-content', $key)[1];

                            $templates[$key] = $header[1];
                        }
                    }
                }
            }

            return $templates;
        });

        add_filter('template_include', function($template) use ($filesystem, $path) {
            if(is_page()) {
                $meta = get_post_meta(get_the_ID());
        
                if(strpos($template, 'web/app') !== false) {
                    // stuff to do on bedrock env
                } elseif (!empty($meta['_wp_page_template'][0]) && $meta['_wp_page_template'][0] != $template && $meta['_wp_page_template'][0] !== 'default') {
                    if($filesystem->exists($path .'/' . $meta['_wp_page_template'][0])) {
                        $template = $path .'/' . $meta['_wp_page_template'][0];
                    } elseif($filesystem->exists($meta['_wp_page_template'][0])) {
                        $template = $meta['_wp_page_template'][0];
                    }
                }
            }

            if(is_single()) {
                global $post;

                $templatePath = $path . '/single-' . $post->post_type . '.php';

                if($filesystem->exists($templatePath)) {
                    $template = $templatePath;
                }
            }

            if(is_archive()) {
                global $wp_query;

                if(empty($wp_query->query['post_type']) && isset($wp_query->query['author_name'])) {
                    $templatePath = $path . '/author.php';

                    if($filesystem->exists($templatePath)) {
                        $template = $templatePath;
                    }
                } else {
                    $templatePath = $path . '/archive-' . $wp_query->query['post_type'] . '.php';

                    if($filesystem->exists($templatePath)) {
                        $template = $templatePath;
                    }
                }
            }
            
            if(count(explode('wp-content', $template)) > 1) $template = 'wp-content' . explode('wp-content', $template)[1];
        
            return $template;
        }, 99);
    }

    public function loadAcf(): void
    {
        $parametersFile = $this->path . '/config/parameters.yaml';
        $acfPath = $this->path . 'config/acf/';

        if(file_exists($parametersFile)) {
            $parameters = Yaml::parseFile($parametersFile);

            if(!empty($parameters['acf_groups']) && is_array($parameters['acf_groups']) && count($parameters['acf_groups'])) {
                if(!empty($_POST['post_name']) && in_array($_POST['post_name'], $parameters['acf_groups'])) {
                    add_filter('acf/settings/save_json', function($paths) use ($acfPath) { return $acfPath; });
                }

                $this->checkAcfEnabled();
            }
        }

        add_filter('acf/settings/load_json', function($paths) use ($acfPath) {
            $paths[] = $acfPath;

            return $paths;
        });

        add_filter('acf/settings/load_json', function($paths) { 
            $filesystem = new Filesystem();
            $finder = new Finder();
            $groups = [];

            foreach($paths as $k => $path) {
                if($filesystem->exists($path . '../../config/acf')) {
                    $parametersFile = $path . '../parameters.yaml';

                    if(!file_exists($parametersFile)) {
                        if($filesystem->exists($path)) {
                            $filesystem->remove($path);
                        }
                        $filesystem->mkdir($path);

                        unset($paths[$k]);
                        continue;
                    } else {
                        $parameters = Yaml::parseFile($parametersFile);

                        if(!empty($parameters['acf_groups']) && is_array($parameters['acf_groups']) && count($parameters['acf_groups'])) {
                            foreach($parameters['acf_groups'] as $group) {
                                if(empty($groups[$group])) {
                                    $groups[$group] = $path . $group . '.json';
                                }
                            }
                        } else {
                            if($filesystem->exists($path)) {
                                $filesystem->remove($path);
                            }
                            $filesystem->mkdir($path);
    
                            unset($paths[$k]);
                            continue;
                        }
                    }
                }
            }

            foreach($paths as $k => $path) {
                if($filesystem->exists($path . '../../config/acf')) {
                    // Nettoyage fichiers
                    $finder->files()->depth(0)->in($path)->name('*.json');
                    if ($finder->hasResults()) {
                        foreach ($finder as $file) {
                            if(!in_array(str_replace('\\', '/', $file->getPathname()), $groups)) {
                                $filesystem->remove($file->getPathname());
                            }
                        }
                    }

                    // Nettoyage configs
                    $parameters = Yaml::parseFile($path . '../parameters.yaml');

                    if(!empty($parameters['acf_groups']) && is_array($parameters['acf_groups']) && count($parameters['acf_groups'])) {
                        $clearedParamAcfGroups = [];
                        foreach($parameters['acf_groups'] as $group) {
                            if(in_array($path . $group . '.json', $groups)) {
                                $clearedParamAcfGroups[] = $group;
                            }
                        }

                        $parameters['acf_groups'] = $clearedParamAcfGroups;

                        $filesystem->dumpFile($path . '../parameters.yaml', Yaml::dump($parameters));
                    }
                }
            }

            return $paths;
        }, 999);
    }

    public function loadServices(): void
    {
        $finder = new Finder();
        $filesystem = new Filesystem();

        $path = $this->path . '/src/Service';

        if($filesystem->exists($path)) {
            $finder->files()->in($path)->name('*.php');

            if ($finder->hasResults()) {
                foreach ($finder as $file) {
                    $className = 'Genesii\\Service\\' . str_replace('.php', '', $file->getFileName());

                    $this->services[] = new $className($this->path);
                }
            }
        }
    }

    public function loadOptionPages(): void
    {
        $finder = new Finder();
        $filesystem = new Filesystem();

        $path = $this->path . '/src/OptionPage';

        if($filesystem->exists($path)) {
            $finder->files()->in($path)->name('*.php');

            if ($finder->hasResults()) {
                $plugin = $this;

                add_action('init', function() use ($plugin, $finder) {
                    $plugin->checkAcfEnabled();

                    foreach ($finder as $file) {
                        $className = 'Genesii\\OptionPage\\' . str_replace('.php', '', $file->getFileName());
    
                        $plugin->optionPages[] = new $className();
                    }
                });
            }
        }
    }

    private function checkAcfEnabled(): void
    {
        add_action('admin_notices', function() {
            if(!is_plugin_active('advanced-custom-fields-pro/acf.php')) {
                $pluginData = get_plugin_data($this->path . str_replace('-', '_', $this->name) . '.php');

                echo '<div class="error notice"><p>L\'extension <b>Advanced Custom Fields Pro</b> doit être activée pour que l\'extension <b>' . $pluginData['Name'] . '</b> puisse fonctionner correctement.</p></div>';
                deactivate_plugins(basename($this->path) . '/' . str_replace('-', '_', $this->name) . '.php');
                echo '<div class="error notice"><p>L\'extension <b>' . $pluginData['Name'] . '</b> a été désactivée pour des raisons de sécurité.</p></div>';
            }
        });
    }
}