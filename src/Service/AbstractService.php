<?php

namespace Genesii\Kernel\Service;

abstract class AbstractService  {

    protected $path;
    
    public function __construct($path) 
    {
        $this->path = $path;
        $this->hooks();
    }

    protected function hooks(): void {}
}