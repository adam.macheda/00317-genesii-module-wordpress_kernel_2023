<?php

namespace Genesii\Kernel\OptionPage;

abstract class AbstractOptionPage {

    public function __construct()
    {
        $optionPage = get_called_class();

        if(!function_exists('acf_add_options_page')) return;

        if(!defined("$optionPage::PARENT_SLUG") || is_null($optionPage::PARENT_SLUG)) {
            acf_add_options_page(array(
                'page_title'    => $optionPage::TITLE,
                'menu_title'    => $optionPage::TITLE,
                'menu_slug'     => $optionPage::SLUG,
                'icon_url'      => defined("$optionPage::ICON") ? $optionPage::ICON : null,
                'redirect'      => defined("$optionPage::REDIRECT") ? $optionPage::REDIRECT : true,
                'position'      => defined("$optionPage::POSITION") ? $optionPage::POSITION : ''
            ));
        } else {
            acf_add_options_sub_page(array(
                'page_title'    => $optionPage::TITLE,
                'menu_title'    => $optionPage::TITLE,
                'parent_slug'   => $optionPage::PARENT_SLUG,
                'position'      => defined("$optionPage::POSITION") ? $optionPage::POSITION : ''
            ));
        }
    }
}