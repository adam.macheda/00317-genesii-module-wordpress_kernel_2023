<?php

namespace Genesii\Kernel\Taxonomy;

abstract class AbstractTaxonomy {

    public static function init(): string 
    {
        $taxo = get_called_class();

        add_action('init', function() use ($taxo) {
            register_taxonomy($taxo::SLUG, $taxo::POST_TYPE, $taxo::args());
        });

        return $taxo;
    }

    public static function args(): array 
    {
        $taxo = get_called_class();

        $args = [
            'hierarchical' => true,
            'meta_box_cb' => defined("$taxo::META_BOX_CB") ? $taxo::META_BOX_CB : null,
            'labels' => [
                'name' => __($taxo::PLURIAL_NAME),
                'singular_name' => __($taxo::SINGULAR_NAME),
                'add_new' => __('Ajouter un' . ($taxo::FEMININE ? 'e' : '') . ' ' . strtolower($taxo::SINGULAR_NAME)),
                'add_new_item' => __('Ajouter un' . ($taxo::FEMININE ? 'e' : '') . ' ' . strtolower($taxo::SINGULAR_NAME)),
                'edit_item' => __('Modifier l' . ($taxo::FEMININE ? 'a' : 'e') . ' ' . strtolower($taxo::SINGULAR_NAME)),
                'new_item' => __('Nouve' . ($taxo::FEMININE ? 'lle' : 'au') . ' ' . strtolower($taxo::SINGULAR_NAME)),
                'all_items' => __('Tou' . ($taxo::FEMININE ? 'tes les' : 's les') . ' ' . strtolower($taxo::PLURIAL_NAME)),
                'view_item' => __('Voir l' . ($taxo::FEMININE ? 'a' : 'e') . ' ' . strtolower($taxo::SINGULAR_NAME)),
                'search_items' => __('Rechercher un' . ($taxo::FEMININE ? 'e' : '') . ' ' . strtolower($taxo::SINGULAR_NAME)),
                'not_found' => __('Aucun résultat'),
                'not_found_in_trash' => __('La corbeille ne contient aucun' . ($taxo::FEMININE ? 'e' : '') . ' ' . strtolower($taxo::SINGULAR_NAME)),
                'parent_item_colon' => __($taxo::SINGULAR_NAME . ' parent' . ($taxo::FEMININE ? 'e' : '')),
                'menu_name' => __($taxo::PLURIAL_NAME)
            ],
            'rewrite' => [
                'slug' => defined("$taxo::REWRITE_SLUG") ? $taxo::REWRITE_SLUG : $taxo::SLUG, // This controls the base slug that will display before each term
                'with_front' => defined("$taxo::REWRITE_WITH_FRONT") ? $taxo::REWRITE_WITH_FRONT : false, // Don't display the category base before "/locations/"
                'hierarchical' => defined("$taxo::REWRITE_HIERARCHICAL") ? $taxo::REWRITE_HIERARCHICAL : true // This will allow URL's like "/locations/boston/cambridge/"
            ],
        ];

        return $args;
    }
}