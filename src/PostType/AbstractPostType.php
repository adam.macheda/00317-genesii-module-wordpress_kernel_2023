<?php

namespace Genesii\Kernel\PostType;

abstract class AbstractPostType {

    private $id, $post;

    public function __construct($postId=null) {

        $this->id = $postId;
        $this->post = get_post($postId);
    }

    public function __set($prop, $val) {

        update_field($prop, $val, $this->id);
    }

    public function __get($prop) {

        if($prop == 'id') return $this->id;
        return get_field($prop, $this->id);
    }

    public function getPost() {

        return $this->post;
    }

    public function delete() {

        wp_delete_post($this->id);
    }

    public function setPostTitle($title) {

        $args = [
            'ID' => $this->id,
            'post_title' => $title
        ];
        wp_update_post($args);
    }

    public function getTitle() {

        return get_the_title($this->getPost());
    }

    public function getPermalink() {

        return get_the_permalink($this->getPost());
    }

    public function getArchivePermalink() {

        return get_post_type_archive_link(get_post_type($this->getPost()));
    }

    public function setMeta($prop, $val) {

        update_post_meta($this->id, $prop, $val);
    }

    public function getMeta($prop) {

        if($prop == 'id') return $this->id;
        return get_post_meta($this->id, $prop, true);
    }

    public static function init(): string 
    {
        $cpt = get_called_class();

        add_action('init', function() use ($cpt) {
            register_post_type($cpt::SLUG, $cpt::args());
            if($cpt::REMOVE_EDITOR) remove_post_type_support($cpt::SLUG, 'editor');
        });

        add_action('save_post', [$cpt, 'onPostSave']);

        $cpt::hooks();

        return $cpt;
    }

    public static function args(): array 
    {
        $cpt = get_called_class();

        $args = [
            'labels' => [
                'name' => __($cpt::PLURIAL_NAME),
                'singular_name' => __($cpt::SINGULAR_NAME),
                'add_new' => __('Ajouter un' . ($cpt::FEMININE ? 'e' : '') . ' ' . strtolower($cpt::SINGULAR_NAME)),
                'add_new_item' => __('Ajouter un' . ($cpt::FEMININE ? 'e' : '') . ' ' . strtolower($cpt::SINGULAR_NAME)),
                'edit_item' => __('Modifier l' . ($cpt::FEMININE ? 'a' : 'e') . ' ' . strtolower($cpt::SINGULAR_NAME)),
                'new_item' => __('Nouve' . ($cpt::FEMININE ? 'lle' : 'au') . ' ' . strtolower($cpt::SINGULAR_NAME)),
                'all_items' => __('Tou' . ($cpt::FEMININE ? 'tes les' : 's les') . ' ' . strtolower($cpt::PLURIAL_NAME)),
                'view_item' => __('Voir l' . ($cpt::FEMININE ? 'a' : 'e') . ' ' . strtolower($cpt::SINGULAR_NAME)),
                'search_items' => __('Rechercher un' . ($cpt::FEMININE ? 'e' : '') . ' ' . strtolower($cpt::SINGULAR_NAME)),
                'not_found' => __('Aucun résultat'),
                'not_found_in_trash' => __('La corbeille ne contient aucun' . ($cpt::FEMININE ? 'e' : '') . ' ' . strtolower($cpt::SINGULAR_NAME)),
                'parent_item_colon' => __($cpt::SINGULAR_NAME . ' parent' . ($cpt::FEMININE ? 'e' : '')),
                'menu_name' => __($cpt::PLURIAL_NAME)
            ],
            'rewrite' => ['slug' => $cpt::REWRITE_SLUG],
            'description' => __('Description'),
            'public' => defined("$cpt::PUBLIC") ? $cpt::PUBLIC : true,
            'publicly_queryable' => defined("$cpt::PUBLICLY_QUERYABLE") ? $cpt::PUBLICLY_QUERYABLE : true,
            'show_ui' => defined("$cpt::SHOW_UI") ? $cpt::SHOW_UI : true,
            'show_in_menu' => defined("$cpt::SHOW_IN_MENU") ? $cpt::SHOW_IN_MENU : true,
            'show_in_rest' => defined("$cpt::SHOW_IN_REST") ? $cpt::SHOW_IN_REST : true,
            'query_var' => defined("$cpt::QUERY_VAR") ? $cpt::QUERY_VAR : true,
            'capability_type' => defined("$cpt::CAPABILITY_TYPE") ? $cpt::CAPABILITY_TYPE : 'post',
            'has_archive' => defined("$cpt::HAS_ARCHIVE") ? $cpt::HAS_ARCHIVE : true,
            'hierarchical' => defined("$cpt::HIERARCHICAL") ? $cpt::HIERARCHICAL : true,
            'menu_position' => $cpt::MENU_POSITION,
            'supports' => defined("$cpt::SUPPORTS") ? $cpt::SUPPORTS : ['title'],
            'menu_icon' => defined("$cpt::MENU_ICON") && trim($cpt::MENU_ICON) != '' ? $cpt::MENU_ICON : null,
        ];

        return $args;
    }

    public static function onPostSave($postId): void
    {
        $cpt = get_called_class();
        
        if (empty($_POST)) {
            return;
        }

        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }

        if (!empty($_POST['post_type']) && $cpt::SLUG == $_POST['post_type']) {
            if (!current_user_can('edit_page', $postId)) {
                return;
            }
        } else {
            if (!current_user_can('edit_post', $postId)) {
                return;
            }
        }

        $cpt::save($postId);
    }
}