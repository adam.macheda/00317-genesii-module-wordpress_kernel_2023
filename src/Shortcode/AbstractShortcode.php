<?php

namespace Genesii\Kernel\Shortcode;

abstract class AbstractShortCode {

    protected $path;

    protected $code;

    public function __construct($path) 
    {   
        $this->path = $path;
        $this->code = get_called_class()::CODE;

        add_shortcode($this->code, [&$this, 'action']);
    }

    public function action($args): string {
        ob_start();

        if(!is_null($args) && !is_array($args)) $args = null;

        $template = $this->path . '/templates/shortcode/' . $this->code . '.php';

        $this->do($args);
        
        if(file_exists($template)) require($template);

        return ob_get_clean();
    }

    protected function do(?array $args): void {}
}