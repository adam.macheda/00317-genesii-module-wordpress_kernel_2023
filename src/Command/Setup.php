<?php

namespace Genesii\Kernel\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

#[AsCommand(
    name: 'setup',
    description: 'Génère les configurations et les fichiers nécessaires au fonctionnement du module, ainsi que le nommage.',
    hidden: false
)]
class Setup extends Command {

    protected $vendor = null;
    protected $fs;

    public function __construct(?string $vendor) 
    {
        $this->vendor = $vendor;
        $this->fs = new Filesystem();

        parent::__construct();
    }

    public function configure(): void
    {
        $this
            ->addArgument('composerArgument1', InputArgument::OPTIONAL)
            ->addArgument('composerArgument2', InputArgument::OPTIONAL)
            ->addArgument('composerArgument3', InputArgument::OPTIONAL)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->title('Assistant d\'installation');

        $nom = $io->askQuestion(new Question('<fg=green>Nom (affiché dans le back office)</>', 'Mon module'));
        $description = $io->askQuestion(new Question('<fg=green>Description (affichée dans le back office)</>', 'Ma description'));
        $version = $io->askQuestion(new Question('<fg=green>Numéro de version (affichée dans le back office)</>', '0.0.0'));
        $assets = $io->askQuestion(new ConfirmationQuestion('Souhaitez-vous intégrer au plugin du <fg=yellow>JS</> et du <fg=yellow>CSS</> personnalisé ?', false));

        if($assets) {
            if(!$this->fs->exists($this->vendor . '/../templates/css')) {
                $this->fs->mkdir($this->vendor . '/../templates/css');
            }

            if(!$this->fs->exists($this->vendor . '/../templates/js')) {
                $this->fs->mkdir($this->vendor . '/../templates/js');
            }

            $this->fs->dumpFile($this->vendor . '/../templates/js/front.js', "// ...\n// ici, les scripts injectés sur le front");
            $this->fs->dumpFile($this->vendor . '/../templates/js/admin.js', "// ...\n// ici, les scripts injectés dans le back (admin WP)");
            $this->fs->dumpFile($this->vendor . '/../templates/css/front.css', "/* ... */\n/* ici, le style appliqué au front */");
            $this->fs->dumpFile($this->vendor . '/../templates/css/admin.css', "/* ... */\n/* ici, le style appliqué à l'admin */");
        }

        $pathFichierModule = $this->vendor . '/../wordpress_module.php';

        if(file_exists($pathFichierModule)) {
            $baseFichierModule = file_get_contents($pathFichierModule);

            $baseFichierModule = str_replace([
                '{wordpress_module_name}',
                '{wordpress_module_description}',
                '{wordpress_module_version}'
            ], [$nom, $description, $version], $baseFichierModule);
    
            $this->fs->dumpFile($pathFichierModule, $baseFichierModule);
    
            $vendor = str_replace('/vendor', '', $this->vendor);
            $vendor = str_replace('\\', '/', $vendor); // Patch Windaube
            $path = explode('/', $vendor);
            
            $moduleName = end($path);
            $moduleName = str_replace('-', '_', $moduleName);
    
            $this->fs->rename($pathFichierModule, $moduleName . '.php');
        } else {
            $io->error('Le module semble déjà avoir été installé !');
        }

        $io->writeln('Utilisez les commandes <fg=yellow>composer</> pour générer les différentes parties du module.');
        $io->writeln('');
        $io->writeln('Commandes disponibles :');

        $io->writeln('   <fg=green>make:post-type</>                      Génère un type de contenu, ses templates et sa classe');
        $io->writeln('   <fg=green>make:taxonomy</>                       Génère une taxonomie pour un type de contenu');
        $io->writeln('   <fg=green>make:shortcode</>                      Génère un shortcode et son template');
        $io->writeln('   <fg=green>make:service</>                        Génère un service (actions branchées à des hooks)');
        $io->writeln('   <fg=green>make:template</>                       Génère un template de page');
        $io->writeln('   <fg=green>make:acf:sync</>                       Sauvegarde un groupe de champs ACF dans le module');
        $io->writeln('   <fg=green>make:acf:option-page</>                    Ajoute une page d\'options pour ACF dans le back office');

        return Command::SUCCESS;
    }
}