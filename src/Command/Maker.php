<?php

namespace Genesii\Kernel\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Yaml;

use Genesii\Kernel\Prototype\ShortcodePrototype;
use Genesii\Kernel\Prototype\TemplatePrototype;
use Genesii\Kernel\Prototype\PostTypePrototype;
use Genesii\Kernel\Prototype\SinglePrototype;
use Genesii\Kernel\Prototype\ArchivePrototype;
use Genesii\Kernel\Prototype\ServicePrototype;
use Genesii\Kernel\Prototype\TaxonomyPrototype;
use Genesii\Kernel\Prototype\OptionPagePrototype;

use Genesii\Kernel\Utils\Slugify;

#[AsCommand(
    name: 'make',
    description: 'Génère les différentes classes utiles à l\'architecture.',
    hidden: false
)]
class Maker extends Command {

    use Slugify;

    const DASHICONS = [
        'dashicons-menu',
        'dashicons-admin-site',
        'dashicons-dashboard',
        'dashicons-admin-post',
        'dashicons-admin-media',
        'dashicons-admin-links',
        'dashicons-admin-page',
        'dashicons-admin-comments',
        'dashicons-admin-appearance',
        'dashicons-admin-plugins',
        'dashicons-admin-users',
        'dashicons-admin-tools',
        'dashicons-admin-settings',
        'dashicons-admin-network',
        'dashicons-admin-home',
        'dashicons-admin-generic',
        'dashicons-admin-collapse',
        'dashicons-welcome-write-blog',
        'dashicons-welcome-add-page',
        'dashicons-welcome-view-site',
        'dashicons-welcome-widgets-menus',
        'dashicons-welcome-comments',
        'dashicons-welcome-learn-more',
        'dashicons-format-aside',
        'dashicons-format-image',
        'dashicons-format-gallery',
        'dashicons-format-video',
        'dashicons-format-status',
        'dashicons-format-quote',
        'dashicons-format-chat',
        'dashicons-format-audio',
        'dashicons-camera',
        'dashicons-images-alt',
        'dashicons-images-alt2',
        'dashicons-video-alt',
        'dashicons-video-alt2',
        'dashicons-video-alt3',
        'dashicons-image-crop',
        'dashicons-image-rotate-left',
        'dashicons-image-rotate-right',
        'dashicons-image-flip-vertical',
        'dashicons-image-flip-horizontal',
        'dashicons-undo',
        'dashicons-redo',
        'dashicons-editor-bold',
        'dashicons-editor-italic',
        'dashicons-editor-ul',
        'dashicons-editor-ol',
        'dashicons-editor-quote',
        'dashicons-editor-alignleft',
        'dashicons-editor-aligncenter',
        'dashicons-editor-alignright',
        'dashicons-editor-insertmore',
        'dashicons-editor-spellcheck',
        'dashicons-editor-distractionfree',
        'dashicons-editor-kitchensink',
        'dashicons-editor-underline',
        'dashicons-editor-justify',
        'dashicons-editor-textcolor',
        'dashicons-editor-paste-word',
        'dashicons-editor-paste-text',
        'dashicons-editor-removeformatting',
        'dashicons-editor-video',
        'dashicons-editor-customchar',
        'dashicons-editor-outdent',
        'dashicons-editor-indent',
        'dashicons-editor-help',
        'dashicons-editor-strikethrough',
        'dashicons-editor-unlink',
        'dashicons-editor-rtl',
        'dashicons-align-left',
        'dashicons-align-right',
        'dashicons-align-center',
        'dashicons-align-none',
        'dashicons-lock',
        'dashicons-calendar',
        'dashicons-visibility',
        'dashicons-post-status',
        'dashicons-edit',
        'dashicons-trash',
        'dashicons-arrow-up',
        'dashicons-arrow-down',
        'dashicons-arrow-right',
        'dashicons-arrow-left',
        'dashicons-arrow-up-alt',
        'dashicons-arrow-down-alt',
        'dashicons-arrow-right-alt',
        'dashicons-arrow-left-alt',
        'dashicons-arrow-up-alt2',
        'dashicons-arrow-down-alt2',
        'dashicons-arrow-right-alt2',
        'dashicons-arrow-left-alt2',
        'dashicons-sort',
        'dashicons-leftright',
        'dashicons-list-view',
        'dashicons-exerpt-view',
        'dashicons-share',
        'dashicons-share-alt',
        'dashicons-share-alt2',
        'dashicons-twitter',
        'dashicons-rss',
        'dashicons-facebook',
        'dashicons-facebook-alt',
        'dashicons-googleplus',
        'dashicons-networking',
        'dashicons-hammer',
        'dashicons-art',
        'dashicons-migrate',
        'dashicons-performance',
        'dashicons-wordpress',
        'dashicons-wordpress-alt',
        'dashicons-pressthis',
        'dashicons-update',
        'dashicons-screenoptions',
        'dashicons-info',
        'dashicons-cart',
        'dashicons-feedback',
        'dashicons-cloud',
        'dashicons-translation',
        'dashicons-tag',
        'dashicons-category',
        'dashicons-yes',
        'dashicons-no',
        'dashicons-no-alt',
        'dashicons-plus',
        'dashicons-minus',
        'dashicons-dismiss',
        'dashicons-marker',
        'dashicons-star-filled',
        'dashicons-star-half',
        'dashicons-star-empty',
        'dashicons-flag',
        'dashicons-location',
        'dashicons-location-alt',
        'dashicons-vault',
        'dashicons-shield',
        'dashicons-shield-alt',
        'dashicons-sos',
        'dashicons-search',
        'dashicons-text-page',
        'dashicons-slides',
        'dashicons-analytics',
        'dashicons-chart-pie',
        'dashicons-chart-bar',
        'dashicons-chart-line',
        'dashicons-chart-area',
        'dashicons-groups',
        'dashicons-businessman',
        'dashicons-businesswoman',
        'dashicons-businessperson',
        'dashicons-id',
        'dashicons-id-alt',
        'dashicons-products',
        'dashicons-awards',
        'dashicons-forms',
        'dashicons-textimonial',
        'dashicons-portfolio',
        'dashicons-book',
        'dashicons-book-alt',
        'dashicons-download',
        'dashicons-upload',
        'dashicons-backup',
        'dashicons-clock',
        'dashicons-lightbulb',
        'dashicons-microphone',
        'dashicons-desktop',
        'dashicons-laptop',
        'dashicons-tablet',
        'dashicons-smartphone',
        'dashicons-phone',
        'dashicons-index-card',
        'dashicons-carrot',
        'dashicons-building',
        'dashicons-store',
        'dashicons-album',
        'dashicons-palm-tree',
        'dashicons-tickets-alt',
        'dashicons-money',
        'dashicons-money-alt',
        'dashicons-smiley',
        'dashicons-thumbs-up',
        'dashicons-thumbs-down',
        'dashicons-layout',
        'dashicons-paperclip',
        'dashicons-color-picker',
        'dashicons-edit-large',
        'dashicons-edit-page',
        'dashicons-airplane',
        'dashicons-bank',
        'dashicons-beer',
        'dashicons-calculator',
        'dashicons-car',
        'dashicons-coffee',
        'dashicons-drumstick',
        'dashicons-food',
        'dashicons-fullscreen-alt',
        'dashicons-fullscreen-exit-alt',
        'dashicons-games',
        'dashicons-hourglass',
        'dashicons-open-folder',
        'dashicons-PDF',
        'dashicons-pets',
        'dashicons-printer',
        'dashicons-privacy',
        'dashicons-superhero',
        'dashicons-superhero-alt',
    ];

    protected $vendor = null;
    protected $fs;

    public function __construct(?string $vendor) 
    {
        $this->vendor = $vendor;
        $this->fs = new Filesystem();

        parent::__construct();
    }

    public function configure(): void
    {
        $this
            ->addArgument('composerCommand', InputArgument::OPTIONAL)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        switch ($input->getArgument('composerCommand')) {
            case 'make:shortcode':
                return $this->shortcode($input, $output);
                break;

            case 'make:template':
                return $this->template($input, $output);
                break;
            
            case 'make:acf:sync':
                return $this->acf($input, $output);
                break;

            case 'make:post-type':
                return $this->postType($input, $output);
                break;

            case 'make:service':
                return $this->service($input, $output);
                break;

            case 'make:taxonomy':
                return $this->taxonomy($input, $output);
                break;

            case 'make:acf:option-page':
                return $this->optionPage($input, $output);
                break;
            
            default:
                return Command::SUCCESS;
                break;
        }
    }

    private function taxonomy(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $basePath = $this->vendor . '/../src/Taxonomy';

        $io->title('Ajouter une taxonomie');

        $cpt = null;
        while (null === $cpt) {
            $question = new Question('<fg=green>Contenu auquel associer cette taxonomy (ex: </><fg=yellow>post</><fg=green>)</>');

            $helper = $this->getHelper('question');
            $cpt = $io->askQuestion($question);

            if(is_null($cpt) || trim($cpt) == '') {
                $io->error(sprintf('Le nom du post type est incorrect.', $cpt));
                $io->writeln('');

                $cpt = null;
            }
        }

        $nom = null;
        while (null === $nom) {
            $question = new Question('<fg=green>Libellé (au singulier, ex: </><fg=yellow>Taille</><fg=green>)</>');

            $helper = $this->getHelper('question');
            $nom = $io->askQuestion($question);

            if (!$this->validateName($nom)) {
                $io->error(sprintf('Le libellé "%s" n\'est pas valide.', $nom));
                $io->writeln('');

                $nom = null;
            }

            if ($nom != null && class_exists('Genesii\Taxonomy\\' . str_replace(' ', '', ucwords($nom)))) {
                $io->error(sprintf('Le libellé "%s" existe déjà.', str_replace(' ', '', ucwords($nom))));
                $io->writeln('');

                $nom = null;
            }
        }

        $pluriel = null;
        while (null === $pluriel) {
            $question = new Question('<fg=green>Libellé au pluriel (ex: </><fg=yellow>Tailles</><fg=green>)</>');

            $helper = $this->getHelper('question');
            $pluriel = $io->askQuestion($question);

            if (!$this->validateName($pluriel)) {
                $io->error(sprintf('Le libellé "%s" n\'est pas valide.', $pluriel));
                $io->writeln('');

                $pluriel = null;
            }
        }

        $question = new ConfirmationQuestion('S\'agit-il d\'un libellé féminin ?', false);

        $helper = $this->getHelper('question');
        $feminin = $io->askQuestion($question);

        $slug = $this->slugify(str_replace(' ', '', $nom));

        $taxonomy = new TaxonomyPrototype();
        $taxonomy->setData([
            'singulier' => $nom,
            'pluriel' => $pluriel,
            'feminin' => $feminin,
            'slug' => $slug,
            'cpt' => $cpt
        ]);
        $taxonomy->setName(str_replace(' ', '', ucwords($nom)));

        if(!$this->fs->exists($basePath)) {
            $this->fs->mkdir($basePath);
        }

        $this->fs->dumpFile($basePath . '/' . $taxonomy->getFileName(), $taxonomy->getPhpCode());

        $this->writeSuccessMessage($io);

        return Command::SUCCESS;
    }

    private function postType(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $basePath = $this->vendor . '/../src/PostType';

        $io->title('Ajouter un type de contenu');

        $nom = null;
        while (null === $nom) {
            $question = new Question('<fg=green>Libellé (au singulier, ex: </><fg=yellow>Bien immobilier</><fg=green>)</>');

            $helper = $this->getHelper('question');
            $nom = $io->askQuestion($question);

            if (!$this->validateName($nom)) {
                $io->error(sprintf('Le libellé "%s" n\'est pas valide.', $nom));
                $io->writeln('');

                $nom = null;
            }

            if ($nom != null && class_exists('Genesii\PostType\\' . str_replace(' ', '', ucwords($nom)))) {
                $io->error(sprintf('Le libellé "%s" existe déjà.', str_replace(' ', '', ucwords($nom))));
                $io->writeln('');

                $nom = null;
            }
        }

        $pluriel = null;
        while (null === $pluriel) {
            $question = new Question('<fg=green>Libellé au pluriel (ex: </><fg=yellow>Biens immobiliers</><fg=green>)</>');

            $helper = $this->getHelper('question');
            $pluriel = $io->askQuestion($question);

            if (!$this->validateName($pluriel)) {
                $io->error(sprintf('Le libellé "%s" n\'est pas valide.', $pluriel));
                $io->writeln('');

                $pluriel = null;
            }
        }

        $question = new ConfirmationQuestion('S\'agit-il d\'un libellé féminin ?', false);

        $helper = $this->getHelper('question');
        $feminin = $io->askQuestion($question);

        $slug = $this->slugify(str_replace(' ', '', $nom));

        $question = new ConfirmationQuestion('Souhaitez-vous utiliser un icone personnalisé pour le menu ?', true);

        $helper = $this->getHelper('question');
        $iconeOuiNon = $io->askQuestion($question);

        $icone = null;

        if($iconeOuiNon) {
            while (null === $icone) {
                $question = new Question('<fg=green>Icône à utiliser dans le menu</>', 'dashicons-admin-post');
                $question->setAutocompleterValues(self::DASHICONS);
    
                $helper = $this->getHelper('question');
                $icone = $io->askQuestion($question);

                if (!in_array($icone, self::DASHICONS)) {
                    $io->error(sprintf('L\'icone "%s" n\'existe pas dans la liste des dashicons de Wordpress.', $icone));
                    $io->writeln('');
    
                    $icone = null;
                }
            }
        }

        $question = new ConfirmationQuestion('Souhaitez-vous générer les templates des pages <fg=yellow>single</> et <fg=yellow>archive</> ?', false);

        $helper = $this->getHelper('question');
        $templates = $io->askQuestion($question);

        $postType = new PostTypePrototype();
        $postType->setData([
            'singulier' => $nom,
            'pluriel' => $pluriel,
            'icone' => $icone,
            'feminin' => $feminin,
            'slug' => $slug,
            'public' => $templates
        ]);
        $postType->setName(str_replace(' ', '', ucwords($nom)));

        $single = new SinglePrototype();
        $single->setName($postType->getName());
        $single->setData([
            'slug' => $slug
        ]);

        $archive = new ArchivePrototype();
        $archive->setName($postType->getName());
        $archive->setData([
            'slug' => $slug
        ]);

        if(!$this->fs->exists($basePath)) {
            $this->fs->mkdir($basePath);
        }

        $this->fs->dumpFile($basePath . '/' . $postType->getFileName(), $postType->getPhpCode());

        if($templates) {
            $templatesPath = $this->vendor . '/../templates/';

            if(!$this->fs->exists($templatesPath)) {
                $this->fs->mkdir($templatesPath);
            }

            $this->fs->dumpFile($templatesPath . $single->getFileName(), $single->getPhpCode());
            $this->fs->dumpFile($templatesPath . $archive->getFileName(), $archive->getPhpCode());
        }

        $this->writeSuccessMessage($io);

        return Command::SUCCESS;
    }

    private function service(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $basePath = $this->vendor . '/../src/Service';

        $io->title('Ajouter un service');

        $nom = null;
        while (null === $nom) {
            $question = new Question('<fg=green>Nom du service (utiliser la camel case, ex: </><fg=yellow>AdminMenuDisabler</><fg=green>)</>');

            $helper = $this->getHelper('question');
            $nom = $io->askQuestion($question);

            if (!$this->validateClassName($nom)) {
                $io->error(sprintf('Le nom "%s" n\'est pas valide.', $nom));
                $io->writeln('');

                $nom = null;
            }

            if ($nom != null && class_exists('Genesii\Service\\' . str_replace(' ', '', ucwords($nom)))) {
                $io->error(sprintf('Le nom de service "%s" est déjà utilisé.', $nom));
                $io->writeln('');

                $nom = null;
            }
        }

        $service = new ServicePrototype();
        $service->setName(str_replace(' ', '', ucwords($nom)));

        if(!$this->fs->exists($basePath)) {
            $this->fs->mkdir($basePath);
        }

        $this->fs->dumpFile($basePath . '/' . $service->getFileName(), $service->getPhpCode());

        $this->writeSuccessMessage($io);

        return Command::SUCCESS;
    }

    private function shortcode(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $basePath = $this->vendor . '/../src/Shortcode';

        $io->title('Ajouter un shortcode');

        $nom = null;
        while (null === $nom) {
            $question = new Question('<fg=green>Nom du shortcode (utiliser la camel case, ex: </><fg=yellow>AfficherCarteGoogle</><fg=green>)</>');

            $helper = $this->getHelper('question');
            $nom = $io->askQuestion($question);

            if (!$this->validateClassName($nom)) {
                $io->error(sprintf('Le nom "%s" n\'est pas valide.', $nom));
                $io->writeln('');

                $nom = null;
            }

            if ($nom != null && class_exists('Genesii\Shortcode\\' . ucfirst($nom))) {
                $io->error(sprintf('Le nom de shortcode "%s" est déjà utilisé.', $nom));
                $io->writeln('');

                $nom = null;
            }
        }

        $code = null;
        while (null === $code) {
            $question = new Question('<fg=green>Code (ex: </><fg=yellow>afficher-carte-google</><fg=green>)</>');

            $helper = $this->getHelper('question');
            $code = $io->askQuestion($question);

            if (trim($this->slugify($code)) == '') {
                $io->error(sprintf('Le code "%s" n\'est pas valide.', $code));
                $io->writeln('');

                $code = null;
            }
        }

        $shortcode = new ShortcodePrototype();
        $shortcode->setName($nom);
        $shortcode->setData([
            'code' => $this->slugify($code)
        ]);

        if(!$this->fs->exists($basePath)) {
            $this->fs->mkdir($basePath);
        }

        $this->fs->dumpFile($basePath . '/' . $shortcode->getFileName(), $shortcode->getPhpCode());
        $this->fs->dumpFile($this->vendor . '/../templates/shortcode/' . $shortcode->get('code') . '.php', "<?php\n// ...");

        $this->writeSuccessMessage($io);

        return Command::SUCCESS;
    }

    private function template(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $basePath = $this->vendor . '/../templates';

        $io->title('Ajouter un modèle de page');

        $nom = null;
        while (null === $nom) {
            $question = new Question('<fg=green>Nom du modèle de page (ex: </><fg=yellow>Mon super gabarit</><fg=green>)</>');

            $helper = $this->getHelper('question');
            $nom = $io->askQuestion($question);

            if (!$this->validateClassName($nom) && trim($this->slugify($nom)) == '') {
                $io->error(sprintf('Le nom "%s" n\'est pas valide.', $nom));
                $io->writeln('');

                $nom = null;
            }

            if($nom != null) {
                $template = new TemplatePrototype();
                $template->setName($nom);

                if(file_exists($basePath . '/' . $template->getFileName())) {
                    $io->error(sprintf('Le template "%s" existe déjà.', $template->getFileName()));
                    $io->writeln('');
                    
                    $nom = null;
                }
            }
        }

        if(!$this->fs->exists($basePath)) {
            $this->fs->mkdir($basePath);
        }

        $this->fs->dumpFile($basePath . '/' . $template->getFileName(), $template->getPhpCode());

        $this->writeSuccessMessage($io);

        return Command::SUCCESS;
    }

    private function acf(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $basePath = $this->vendor . '/../config';

        $io->title('Synchroniser des champs ACF');

        $nom = null;
        while (null === $nom) {
            $question = new Question('<fg=green>Nom du groupe de champs à sauvegarder dans le module (ex: </><fg=yellow>group_6458cf3195af9</><fg=green>)</>');

            $helper = $this->getHelper('question');
            $nom = $io->askQuestion($question);

            if (trim($nom) == '') {
                $io->error(sprintf('Le nom "%s" n\'est pas valide.', $nom));
                $io->writeln('');

                $nom = null;
            }
        }

        if(!$this->fs->exists($basePath . '/acf')) {
            $this->fs->mkdir($basePath . '/acf');
        }

        if(!file_exists($basePath . '/parameters.yaml')) {
            $this->fs->dumpFile($basePath . '/parameters.yaml', '');
        }
        
        $parameters = Yaml::parseFile($basePath . '/parameters.yaml');

        if(empty($parameters['acf_groups'])) {
            $parameters['acf_groups'] = [];
        }

        if(!in_array($nom, $parameters['acf_groups'])) {
            $parameters['acf_groups'][] = $nom;
        }

        $this->fs->dumpFile($basePath . '/parameters.yaml', Yaml::dump($parameters));

        $this->writeSuccessMessage($io, false);

        return Command::SUCCESS;
    }

    private function optionPage(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $basePath = $this->vendor . '/../src/OptionPage';

        $io->title('Ajouter une page d\'options ACF');

        $nom = null;
        while (null === $nom) {
            $question = new Question('<fg=green>Nom de la classe (utiliser la camel case, ex: </><fg=yellow>MesOptions</><fg=green>)</>');

            $helper = $this->getHelper('question');
            $nom = $io->askQuestion($question);

            if (!$this->validateClassName($nom)) {
                $io->error(sprintf('Le nom "%s" n\'est pas valide.', $nom));
                $io->writeln('');

                $nom = null;
            }

            if ($nom != null && class_exists('Genesii\OptionPage\\' . ucfirst($nom))) {
                $io->error(sprintf('Le nom de shortcode "%s" est déjà utilisé.', $nom));
                $io->writeln('');

                $nom = null;
            }
        }

        $menu = null;
        while (null === $menu) {
            $question = new Question('<fg=green>Entrée dans le menu (ex: </><fg=yellow>Mes options</><fg=green>)</>');

            $helper = $this->getHelper('question');
            $menu = $io->askQuestion($question);

            if (trim($menu) == '') {
                $io->error(sprintf('Le libellé "%s" n\'est pas valide.', $menu));
                $io->writeln('');

                $menu = null;
            }
        }

        $slug = $this->camelToSlug($nom);

        $parentSlug = null;

        $question = new ConfirmationQuestion('S\'agit-il d\'une sous-page d\'options ?', false);

        $helper = $this->getHelper('question');
        $sousPage = $io->askQuestion($question);

        if($sousPage) {
            while (null === $parentSlug) {
                $question = new Question('<fg=green>Slug de la page d\'options parente (ex: </><fg=yellow>mes-options-parentes</><fg=green>)</>');

                $helper = $this->getHelper('question');
                $parentSlug = $io->askQuestion($question);

                if (trim($parentSlug) == '') {
                    $io->error(sprintf('Le slug "%s" n\'est pas valide.', $parentSlug));
                    $io->writeln('');

                    $parentSlug = null;
                }
            }
        }

        $optionPage = new OptionPagePrototype();
        $optionPage->setName($nom);
        $optionPage->setData([
            'parent_slug' => $parentSlug,
            'slug' => $slug,
            'title' => $menu
        ]);

        if(!$this->fs->exists($basePath)) {
            $this->fs->mkdir($basePath);
        }

        $this->fs->dumpFile($basePath . '/' . $optionPage->getFileName(), $optionPage->getPhpCode());

        $this->writeSuccessMessage($io);

        return Command::SUCCESS;
    }

    private function validateClassName($input) 
    {
        if(is_null($input) || trim($input) == '') return false;

        return preg_match(
            '/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*(\\[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*)*$/', 
            $input
        );
    }

    private function validateName($input)
    {
        if(is_null($input) || trim($input) == '') return false;

        return preg_match('/^[a-zA-Z0-9_\x\s]+$/', $input);
    }

    private function writeSuccessMessage(SymfonyStyle $io, $creation=true)
    {
        $io->newLine();

        if($creation) {
            $io->writeln(' <bg=green;fg=white>                                </>');
            $io->writeln(' <bg=green;fg=white>       Création réussie !       </>');
            $io->writeln(' <bg=green;fg=white>                                </>');
        } else {
            $io->writeln(' <bg=green;fg=white>                          </>');
            $io->writeln(' <bg=green;fg=white>       Enregistré !       </>');
            $io->writeln(' <bg=green;fg=white>                          </>');
        }
        $io->newLine();
    }
}